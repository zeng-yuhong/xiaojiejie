import os
import time
import random
import json
from faceAPI import faceScore
from faceAPI import pngTojpg
adbShell = "adb shell  {cmdStr}"


def execute(cmd):
    str = adbShell.format(cmdStr=cmd)
    print(str)
    os.system(str)

def next():
    x1 = str(random.randint(200, 300))
    y1 = str(random.randint(700, 800))
    x2 = str(random.randint(200, 300))
    y2 = str(random.randint(150, 200))
    time = str(random.randint(50, 100))
    execute("input swipe " + x1 + ' ' + y1 + ' ' + x2 + ' ' + y2 + ' ' + time)  # 滑动

def like():
    execute("input tap 510 604")  # 点赞
    print('like')

# 保存截图到我的电脑图片目录下
def savePic():
    os.system('adb shell /system/bin/screencap -p /sdcard/screenshot.png')
    os.system('adb pull /sdcard/screenshot.png C:/Users/user/Desktop/myTest/image')
   

if __name__ == '__main__':
    while True:
        next()
        temp_time = random.randint(3,5)
        time.sleep(temp_time)
        savePic()
        pngTojpg('./image/')
        #scoreObj是字典类型,用get方法获取value
        scoreObj = json.loads(faceScore('','./image/screenshot.jpg'))
        # print(scoreObj.get('FaceInfos'))
        if(scoreObj.get('FaceInfos')):
            sex = scoreObj.get('FaceInfos')[0].get('FaceAttributesInfo').get('Gender')  #0,1,2    小姐姐<=1
            age = scoreObj.get('FaceInfos')[0].get('FaceAttributesInfo').get('Age') 
            beauty = scoreObj.get('FaceInfos')[0].get('FaceAttributesInfo').get('Beauty') 
            print(sex,age,beauty)
            if(sex <= 1 and (age>=16 or age<=28) and beauty>=60):
                like()
        else:
            continue