#  python刷抖音小姐姐

#### 介绍
基于adb命令调试程序，调用腾讯云人脸识别分析接口，使用python语言编写，实现了手机模拟器或真机自动刷抖音，自动给高颜值小姐姐点赞，让抖音大数据记住你，抖音养号神器

#### 软件架构
软件架构说明


#### 安装教程

1.  adb命令环境
2.  夜神模拟器，真机
3.  python运行环境

#### 使用说明

1.  入口文件为main.py,文件里所设计的路径需要根据自己的实际情况而定
2.  需要自己在腾讯云申请密钥，密钥可前往https://console.cloud.tencent.com/cam/capi网站进行获取
3.  需要在faceAPI.py文件中配置自己的密钥

#### 参与贡献

1.  Fork 本仓库
2.  新建 Feat_xxx 分支
3.  提交代码
4.  新建 Pull Request


#### 特技

1.  使用 Readme\_XXX.md 来支持不同的语言，例如 Readme\_en.md, Readme\_zh.md
2.  Gitee 官方博客 [blog.gitee.com](https://blog.gitee.com)
3.  你可以 [https://gitee.com/explore](https://gitee.com/explore) 这个地址来了解 Gitee 上的优秀开源项目
4.  [GVP](https://gitee.com/gvp) 全称是 Gitee 最有价值开源项目，是综合评定出的优秀开源项目
5.  Gitee 官方提供的使用手册 [https://gitee.com/help](https://gitee.com/help)
6.  Gitee 封面人物是一档用来展示 Gitee 会员风采的栏目 [https://gitee.com/gitee-stars/](https://gitee.com/gitee-stars/)
